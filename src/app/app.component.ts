import { style } from '@angular/animations';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  public index = 0;
  public interval;
  public tempTitle; tempAuthor; tempDescr;
  public tempValue;
  public books = [

    { image_src: 'https://cdn.ozone.bg/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/d/o/355b81683411e77a8fda07446fdb1568/don-kihot-30.jpg', title: 'Дон Кихот', description: 'Провинциалният благородник Дон Кихот прекарва цялото си време в четене на рицарски романи. Те толкова завладяват съзнанието му, че накрая започва да се отъждествява с техните герои и тръгва да търси приключения.', author: 'Мигел де Сервантес', current_rating: 0.0, rating: 0.0, ratingCount: 0 },
    { image_src: 'https://i3.helikon.bg/products/0716/21/210716/210716_b.jpg', title: 'Алхимикът', description: 'Историята разказва за пастира Сантяго, който тръгва от родната си Испания към египетската пустиня, за да търси съкровище, заровено до Пирамидите.  По пътя си той среща гадателка, крадец, войни, момиче, и един алхимик.   Всички те упътват Сантяго накъде да върви, но никой не знае какво е съкровището. За да го открие, младият пастир трябва да се научи да разчита поличбите и да се вслушва в сърцето си. ', author: 'Паулу Коелю', current_rating: 0.0, rating: 0.0, ratingCount: 0 },
    { image_src: 'https://assets2.chitanka.info/thumb/book-cover/00/19.max.jpg', title: 'Франкенщайн', description: ' Разказва историята на талантливия учен Виктор Франкенщайн, който се е научил да съживява материята. Създавайки чудовище от части на мъртви тела, той получава своя маниакален преследвач.', author: 'Мери Шели', current_rating: 0.0, rating: 0.0, ratingCount: 0 },
    { image_src: 'https://cdn.ozone.bg/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/p/r/10de97cec1b54c016a419347253ade96/prestaplenie-i-nakazanie-yubileyno-izdanie-30.jpg', title: 'Престъпление и наказание', description: 'Беден студент на име Родион Разколников се бори със собственото си нещастие. Той решава да убие възрастна жена, притежателка на заложна къща, но след това страда от престъплението, което е извършил. ', author: 'Фьодор Достоевски', current_rating: 0.0, rating: 0.0, ratingCount: 0 },
    { image_src: 'https://ciela.bg/wp-content/uploads/2022/12/9789542840886_-_.jpg', title: 'Портретът на Дориан Грей', description: 'Един магически портрет започва да старее, докато младежът, нарисуван на него, запазва красотата и младостта си.  В отчаян опит да спаси себе си, младият човек пада по-ниско от най-окаяния жител на викториански Лондон.  Ще спре ли портретът да погрознява и да остарява, изобразявайки вътрешната му същност? ', author: 'Оскар Уайлд', current_rating: 0.0, rating: 0.0, ratingCount: 0 },
  ]
  public rating = 0.0;
  public gfg(n) {


    this.books[this.index].ratingCount++;
    this.books[this.index].rating += n;
    this.books[this.index].current_rating = Math.round(((this.books[this.index].rating / this.books[this.index].ratingCount) + Number.EPSILON) * 100) / 100;
    this.rating = this.books[this.index].rating / this.books[this.index].ratingCount;
    let array = document.getElementsByClassName('star');

    for (let j = 0; j <= this.books[this.index].current_rating - 1; j++) {

      array[j].setAttribute("style", "color:yellow");
    }
    this.submitChanges();

  }


  public processNextBook() {
    this.interval = setTimeout(() => {
      this.index++;
      if (this.index >= this.books.length) {
        this.index = 0;
      }
      this.rating = 0.0;
      let array = document.getElementsByClassName('star');

      if (this.books[this.index].current_rating == 0.0) {
        for (let i = 0; i <= array.length; i++) {
          array[i].setAttribute("style", "color:black");
        }
      }
      else {
        for (let i = 0; i <= array.length; i++) {
          if (i < this.books[this.index].current_rating) array[i].setAttribute("style", "color:yellow");
          else array[i].setAttribute("style", "color:black");
        }
      }
    }, 1500);
    this.resetTempData();
  }

  public submitChanges() {

    if (this.tempAuthor != null || this.tempDescr != null || this.tempTitle != null) {

      this.books[this.index].title = this.tempTitle.value;

      this.books[this.index].description = this.tempDescr.value;

      this.books[this.index].author = this.tempAuthor.value;

      this.resetTempData();
    }
    let inputs = document.getElementsByClassName('input');
    for (let i = 0; i < inputs.length; i++)inputs[i].innerHTML = "";
  }

  public changeTitle(input) {
    this.tempTitle = input.target;
  }
  public changeDescription(input) {
    this.tempDescr = input.target;
  }
  public changeAuthor(input) {
    this.tempAuthor = input.target;
  }
  private resetTempData() {
    this.tempTitle = null;
    this.tempDescr = null;
    this.tempAuthor = null;
    this.tempValue = null;

  }

}
